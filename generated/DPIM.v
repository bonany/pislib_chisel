module DPIM(
  input         clock,
  input         reset,
  input  [11:0] io_addr,
  input  [31:0] io_d,
  output [31:0] io_q,
  input         io_we,
  input         io_cme,
  input  [31:0] io_cmIn_0,
  input  [31:0] io_cmIn_1,
  input  [31:0] io_cmIn_2,
  input  [31:0] io_cmIn_3,
  input  [31:0] io_cmIn_4,
  input  [31:0] io_cmIn_5,
  input  [31:0] io_cmIn_6,
  input  [31:0] io_cmIn_7,
  input  [31:0] io_cmIn_8,
  input  [31:0] io_cmIn_9,
  input  [31:0] io_cmIn_10,
  input  [31:0] io_cmIn_11,
  input  [31:0] io_cmIn_12,
  input  [31:0] io_cmIn_13,
  input  [31:0] io_cmIn_14,
  input  [31:0] io_cmIn_15,
  input  [31:0] io_cmIn_16,
  input  [31:0] io_cmIn_17,
  input  [31:0] io_cmIn_18,
  input  [31:0] io_cmIn_19,
  input  [31:0] io_cmIn_20,
  input  [31:0] io_cmIn_21,
  input  [31:0] io_cmIn_22,
  input  [31:0] io_cmIn_23,
  input  [31:0] io_cmIn_24,
  input  [31:0] io_cmIn_25,
  input  [31:0] io_cmIn_26,
  input  [31:0] io_cmIn_27,
  input  [31:0] io_cmIn_28,
  input  [31:0] io_cmIn_29,
  input  [31:0] io_cmIn_30,
  input  [31:0] io_cmIn_31,
  output [31:0] io_cmOut_0,
  output [31:0] io_cmOut_1,
  output [31:0] io_cmOut_2,
  output [31:0] io_cmOut_3,
  output [31:0] io_cmOut_4,
  output [31:0] io_cmOut_5,
  output [31:0] io_cmOut_6,
  output [31:0] io_cmOut_7,
  output [31:0] io_cmOut_8,
  output [31:0] io_cmOut_9,
  output [31:0] io_cmOut_10,
  output [31:0] io_cmOut_11,
  output [31:0] io_cmOut_12,
  output [31:0] io_cmOut_13,
  output [31:0] io_cmOut_14,
  output [31:0] io_cmOut_15,
  output [31:0] io_cmOut_16,
  output [31:0] io_cmOut_17,
  output [31:0] io_cmOut_18,
  output [31:0] io_cmOut_19,
  output [31:0] io_cmOut_20,
  output [31:0] io_cmOut_21,
  output [31:0] io_cmOut_22,
  output [31:0] io_cmOut_23,
  output [31:0] io_cmOut_24,
  output [31:0] io_cmOut_25,
  output [31:0] io_cmOut_26,
  output [31:0] io_cmOut_27,
  output [31:0] io_cmOut_28,
  output [31:0] io_cmOut_29,
  output [31:0] io_cmOut_30,
  output [31:0] io_cmOut_31,
  input  [1:0]  io_func
);
`ifdef RANDOMIZE_MEM_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_MEM_INIT
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
`endif // RANDOMIZE_REG_INIT
  reg [31:0] mem [0:4095]; // @[DPIM.scala 36:24]
  wire  mem_b_0_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_0_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_0_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_1_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_1_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_1_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_2_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_2_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_2_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_3_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_3_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_3_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_4_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_4_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_4_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_5_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_5_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_5_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_6_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_6_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_6_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_7_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_7_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_7_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_8_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_8_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_8_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_9_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_9_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_9_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_10_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_10_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_10_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_11_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_11_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_11_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_12_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_12_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_12_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_13_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_13_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_13_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_14_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_14_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_14_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_15_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_15_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_15_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_16_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_16_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_16_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_17_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_17_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_17_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_18_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_18_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_18_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_19_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_19_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_19_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_20_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_20_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_20_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_21_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_21_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_21_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_22_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_22_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_22_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_23_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_23_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_23_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_24_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_24_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_24_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_25_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_25_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_25_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_26_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_26_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_26_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_27_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_27_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_27_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_28_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_28_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_28_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_29_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_29_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_29_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_30_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_30_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_30_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_b_31_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_b_31_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_b_31_MPORT_data; // @[DPIM.scala 36:24]
  wire  mem_io_q_MPORT_en; // @[DPIM.scala 36:24]
  wire [11:0] mem_io_q_MPORT_addr; // @[DPIM.scala 36:24]
  wire [31:0] mem_io_q_MPORT_data; // @[DPIM.scala 36:24]
  wire [31:0] mem_MPORT_data; // @[DPIM.scala 36:24]
  wire [11:0] mem_MPORT_addr; // @[DPIM.scala 36:24]
  wire  mem_MPORT_mask; // @[DPIM.scala 36:24]
  wire  mem_MPORT_en; // @[DPIM.scala 36:24]
  reg  mem_b_0_MPORT_en_pipe_0;
  reg [11:0] mem_b_0_MPORT_addr_pipe_0;
  reg  mem_b_1_MPORT_en_pipe_0;
  reg [11:0] mem_b_1_MPORT_addr_pipe_0;
  reg  mem_b_2_MPORT_en_pipe_0;
  reg [11:0] mem_b_2_MPORT_addr_pipe_0;
  reg  mem_b_3_MPORT_en_pipe_0;
  reg [11:0] mem_b_3_MPORT_addr_pipe_0;
  reg  mem_b_4_MPORT_en_pipe_0;
  reg [11:0] mem_b_4_MPORT_addr_pipe_0;
  reg  mem_b_5_MPORT_en_pipe_0;
  reg [11:0] mem_b_5_MPORT_addr_pipe_0;
  reg  mem_b_6_MPORT_en_pipe_0;
  reg [11:0] mem_b_6_MPORT_addr_pipe_0;
  reg  mem_b_7_MPORT_en_pipe_0;
  reg [11:0] mem_b_7_MPORT_addr_pipe_0;
  reg  mem_b_8_MPORT_en_pipe_0;
  reg [11:0] mem_b_8_MPORT_addr_pipe_0;
  reg  mem_b_9_MPORT_en_pipe_0;
  reg [11:0] mem_b_9_MPORT_addr_pipe_0;
  reg  mem_b_10_MPORT_en_pipe_0;
  reg [11:0] mem_b_10_MPORT_addr_pipe_0;
  reg  mem_b_11_MPORT_en_pipe_0;
  reg [11:0] mem_b_11_MPORT_addr_pipe_0;
  reg  mem_b_12_MPORT_en_pipe_0;
  reg [11:0] mem_b_12_MPORT_addr_pipe_0;
  reg  mem_b_13_MPORT_en_pipe_0;
  reg [11:0] mem_b_13_MPORT_addr_pipe_0;
  reg  mem_b_14_MPORT_en_pipe_0;
  reg [11:0] mem_b_14_MPORT_addr_pipe_0;
  reg  mem_b_15_MPORT_en_pipe_0;
  reg [11:0] mem_b_15_MPORT_addr_pipe_0;
  reg  mem_b_16_MPORT_en_pipe_0;
  reg [11:0] mem_b_16_MPORT_addr_pipe_0;
  reg  mem_b_17_MPORT_en_pipe_0;
  reg [11:0] mem_b_17_MPORT_addr_pipe_0;
  reg  mem_b_18_MPORT_en_pipe_0;
  reg [11:0] mem_b_18_MPORT_addr_pipe_0;
  reg  mem_b_19_MPORT_en_pipe_0;
  reg [11:0] mem_b_19_MPORT_addr_pipe_0;
  reg  mem_b_20_MPORT_en_pipe_0;
  reg [11:0] mem_b_20_MPORT_addr_pipe_0;
  reg  mem_b_21_MPORT_en_pipe_0;
  reg [11:0] mem_b_21_MPORT_addr_pipe_0;
  reg  mem_b_22_MPORT_en_pipe_0;
  reg [11:0] mem_b_22_MPORT_addr_pipe_0;
  reg  mem_b_23_MPORT_en_pipe_0;
  reg [11:0] mem_b_23_MPORT_addr_pipe_0;
  reg  mem_b_24_MPORT_en_pipe_0;
  reg [11:0] mem_b_24_MPORT_addr_pipe_0;
  reg  mem_b_25_MPORT_en_pipe_0;
  reg [11:0] mem_b_25_MPORT_addr_pipe_0;
  reg  mem_b_26_MPORT_en_pipe_0;
  reg [11:0] mem_b_26_MPORT_addr_pipe_0;
  reg  mem_b_27_MPORT_en_pipe_0;
  reg [11:0] mem_b_27_MPORT_addr_pipe_0;
  reg  mem_b_28_MPORT_en_pipe_0;
  reg [11:0] mem_b_28_MPORT_addr_pipe_0;
  reg  mem_b_29_MPORT_en_pipe_0;
  reg [11:0] mem_b_29_MPORT_addr_pipe_0;
  reg  mem_b_30_MPORT_en_pipe_0;
  reg [11:0] mem_b_30_MPORT_addr_pipe_0;
  reg  mem_b_31_MPORT_en_pipe_0;
  reg [11:0] mem_b_31_MPORT_addr_pipe_0;
  reg  mem_io_q_MPORT_en_pipe_0;
  reg [11:0] mem_io_q_MPORT_addr_pipe_0;
  wire [11:0] _b_0_T = io_addr & 12'h7f; // @[DPIM.scala 42:33]
  wire [31:0] a_0 = io_cme ? io_cmIn_0 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_0 = io_cme ? mem_b_0_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_0_T = a_0 * b_0; // @[DPIM.scala 44:22]
  wire [31:0] _c_0_T_1 = a_0 & b_0; // @[DPIM.scala 46:22]
  wire [31:0] _c_0_T_2 = a_0 | b_0; // @[DPIM.scala 48:22]
  wire [31:0] _c_0_T_3 = a_0 ^ b_0; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_4 = io_func == 2'h2 ? _c_0_T_2 : _c_0_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_5 = io_func == 2'h1 ? _c_0_T_1 : _GEN_4; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_6 = io_func == 2'h0 ? _c_0_T : {{32'd0}, _GEN_5}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_1 = io_cme ? io_cmIn_1 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_1 = io_cme ? mem_b_1_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_1_T = a_1 * b_1; // @[DPIM.scala 44:22]
  wire [31:0] _c_1_T_1 = a_1 & b_1; // @[DPIM.scala 46:22]
  wire [31:0] _c_1_T_2 = a_1 | b_1; // @[DPIM.scala 48:22]
  wire [31:0] _c_1_T_3 = a_1 ^ b_1; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_9 = io_func == 2'h2 ? _c_1_T_2 : _c_1_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_10 = io_func == 2'h1 ? _c_1_T_1 : _GEN_9; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_11 = io_func == 2'h0 ? _c_1_T : {{32'd0}, _GEN_10}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_2 = io_cme ? io_cmIn_2 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_2 = io_cme ? mem_b_2_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_2_T = a_2 * b_2; // @[DPIM.scala 44:22]
  wire [31:0] _c_2_T_1 = a_2 & b_2; // @[DPIM.scala 46:22]
  wire [31:0] _c_2_T_2 = a_2 | b_2; // @[DPIM.scala 48:22]
  wire [31:0] _c_2_T_3 = a_2 ^ b_2; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_14 = io_func == 2'h2 ? _c_2_T_2 : _c_2_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_15 = io_func == 2'h1 ? _c_2_T_1 : _GEN_14; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_16 = io_func == 2'h0 ? _c_2_T : {{32'd0}, _GEN_15}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_3 = io_cme ? io_cmIn_3 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_3 = io_cme ? mem_b_3_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_3_T = a_3 * b_3; // @[DPIM.scala 44:22]
  wire [31:0] _c_3_T_1 = a_3 & b_3; // @[DPIM.scala 46:22]
  wire [31:0] _c_3_T_2 = a_3 | b_3; // @[DPIM.scala 48:22]
  wire [31:0] _c_3_T_3 = a_3 ^ b_3; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_19 = io_func == 2'h2 ? _c_3_T_2 : _c_3_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_20 = io_func == 2'h1 ? _c_3_T_1 : _GEN_19; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_21 = io_func == 2'h0 ? _c_3_T : {{32'd0}, _GEN_20}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_4 = io_cme ? io_cmIn_4 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_4 = io_cme ? mem_b_4_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_4_T = a_4 * b_4; // @[DPIM.scala 44:22]
  wire [31:0] _c_4_T_1 = a_4 & b_4; // @[DPIM.scala 46:22]
  wire [31:0] _c_4_T_2 = a_4 | b_4; // @[DPIM.scala 48:22]
  wire [31:0] _c_4_T_3 = a_4 ^ b_4; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_24 = io_func == 2'h2 ? _c_4_T_2 : _c_4_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_25 = io_func == 2'h1 ? _c_4_T_1 : _GEN_24; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_26 = io_func == 2'h0 ? _c_4_T : {{32'd0}, _GEN_25}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_5 = io_cme ? io_cmIn_5 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_5 = io_cme ? mem_b_5_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_5_T = a_5 * b_5; // @[DPIM.scala 44:22]
  wire [31:0] _c_5_T_1 = a_5 & b_5; // @[DPIM.scala 46:22]
  wire [31:0] _c_5_T_2 = a_5 | b_5; // @[DPIM.scala 48:22]
  wire [31:0] _c_5_T_3 = a_5 ^ b_5; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_29 = io_func == 2'h2 ? _c_5_T_2 : _c_5_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_30 = io_func == 2'h1 ? _c_5_T_1 : _GEN_29; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_31 = io_func == 2'h0 ? _c_5_T : {{32'd0}, _GEN_30}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_6 = io_cme ? io_cmIn_6 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_6 = io_cme ? mem_b_6_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_6_T = a_6 * b_6; // @[DPIM.scala 44:22]
  wire [31:0] _c_6_T_1 = a_6 & b_6; // @[DPIM.scala 46:22]
  wire [31:0] _c_6_T_2 = a_6 | b_6; // @[DPIM.scala 48:22]
  wire [31:0] _c_6_T_3 = a_6 ^ b_6; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_34 = io_func == 2'h2 ? _c_6_T_2 : _c_6_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_35 = io_func == 2'h1 ? _c_6_T_1 : _GEN_34; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_36 = io_func == 2'h0 ? _c_6_T : {{32'd0}, _GEN_35}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_7 = io_cme ? io_cmIn_7 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_7 = io_cme ? mem_b_7_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_7_T = a_7 * b_7; // @[DPIM.scala 44:22]
  wire [31:0] _c_7_T_1 = a_7 & b_7; // @[DPIM.scala 46:22]
  wire [31:0] _c_7_T_2 = a_7 | b_7; // @[DPIM.scala 48:22]
  wire [31:0] _c_7_T_3 = a_7 ^ b_7; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_39 = io_func == 2'h2 ? _c_7_T_2 : _c_7_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_40 = io_func == 2'h1 ? _c_7_T_1 : _GEN_39; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_41 = io_func == 2'h0 ? _c_7_T : {{32'd0}, _GEN_40}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_8 = io_cme ? io_cmIn_8 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_8 = io_cme ? mem_b_8_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_8_T = a_8 * b_8; // @[DPIM.scala 44:22]
  wire [31:0] _c_8_T_1 = a_8 & b_8; // @[DPIM.scala 46:22]
  wire [31:0] _c_8_T_2 = a_8 | b_8; // @[DPIM.scala 48:22]
  wire [31:0] _c_8_T_3 = a_8 ^ b_8; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_44 = io_func == 2'h2 ? _c_8_T_2 : _c_8_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_45 = io_func == 2'h1 ? _c_8_T_1 : _GEN_44; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_46 = io_func == 2'h0 ? _c_8_T : {{32'd0}, _GEN_45}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_9 = io_cme ? io_cmIn_9 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_9 = io_cme ? mem_b_9_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_9_T = a_9 * b_9; // @[DPIM.scala 44:22]
  wire [31:0] _c_9_T_1 = a_9 & b_9; // @[DPIM.scala 46:22]
  wire [31:0] _c_9_T_2 = a_9 | b_9; // @[DPIM.scala 48:22]
  wire [31:0] _c_9_T_3 = a_9 ^ b_9; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_49 = io_func == 2'h2 ? _c_9_T_2 : _c_9_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_50 = io_func == 2'h1 ? _c_9_T_1 : _GEN_49; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_51 = io_func == 2'h0 ? _c_9_T : {{32'd0}, _GEN_50}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_10 = io_cme ? io_cmIn_10 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_10 = io_cme ? mem_b_10_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_10_T = a_10 * b_10; // @[DPIM.scala 44:22]
  wire [31:0] _c_10_T_1 = a_10 & b_10; // @[DPIM.scala 46:22]
  wire [31:0] _c_10_T_2 = a_10 | b_10; // @[DPIM.scala 48:22]
  wire [31:0] _c_10_T_3 = a_10 ^ b_10; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_54 = io_func == 2'h2 ? _c_10_T_2 : _c_10_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_55 = io_func == 2'h1 ? _c_10_T_1 : _GEN_54; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_56 = io_func == 2'h0 ? _c_10_T : {{32'd0}, _GEN_55}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_11 = io_cme ? io_cmIn_11 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_11 = io_cme ? mem_b_11_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_11_T = a_11 * b_11; // @[DPIM.scala 44:22]
  wire [31:0] _c_11_T_1 = a_11 & b_11; // @[DPIM.scala 46:22]
  wire [31:0] _c_11_T_2 = a_11 | b_11; // @[DPIM.scala 48:22]
  wire [31:0] _c_11_T_3 = a_11 ^ b_11; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_59 = io_func == 2'h2 ? _c_11_T_2 : _c_11_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_60 = io_func == 2'h1 ? _c_11_T_1 : _GEN_59; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_61 = io_func == 2'h0 ? _c_11_T : {{32'd0}, _GEN_60}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_12 = io_cme ? io_cmIn_12 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_12 = io_cme ? mem_b_12_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_12_T = a_12 * b_12; // @[DPIM.scala 44:22]
  wire [31:0] _c_12_T_1 = a_12 & b_12; // @[DPIM.scala 46:22]
  wire [31:0] _c_12_T_2 = a_12 | b_12; // @[DPIM.scala 48:22]
  wire [31:0] _c_12_T_3 = a_12 ^ b_12; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_64 = io_func == 2'h2 ? _c_12_T_2 : _c_12_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_65 = io_func == 2'h1 ? _c_12_T_1 : _GEN_64; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_66 = io_func == 2'h0 ? _c_12_T : {{32'd0}, _GEN_65}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_13 = io_cme ? io_cmIn_13 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_13 = io_cme ? mem_b_13_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_13_T = a_13 * b_13; // @[DPIM.scala 44:22]
  wire [31:0] _c_13_T_1 = a_13 & b_13; // @[DPIM.scala 46:22]
  wire [31:0] _c_13_T_2 = a_13 | b_13; // @[DPIM.scala 48:22]
  wire [31:0] _c_13_T_3 = a_13 ^ b_13; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_69 = io_func == 2'h2 ? _c_13_T_2 : _c_13_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_70 = io_func == 2'h1 ? _c_13_T_1 : _GEN_69; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_71 = io_func == 2'h0 ? _c_13_T : {{32'd0}, _GEN_70}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_14 = io_cme ? io_cmIn_14 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_14 = io_cme ? mem_b_14_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_14_T = a_14 * b_14; // @[DPIM.scala 44:22]
  wire [31:0] _c_14_T_1 = a_14 & b_14; // @[DPIM.scala 46:22]
  wire [31:0] _c_14_T_2 = a_14 | b_14; // @[DPIM.scala 48:22]
  wire [31:0] _c_14_T_3 = a_14 ^ b_14; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_74 = io_func == 2'h2 ? _c_14_T_2 : _c_14_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_75 = io_func == 2'h1 ? _c_14_T_1 : _GEN_74; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_76 = io_func == 2'h0 ? _c_14_T : {{32'd0}, _GEN_75}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_15 = io_cme ? io_cmIn_15 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_15 = io_cme ? mem_b_15_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_15_T = a_15 * b_15; // @[DPIM.scala 44:22]
  wire [31:0] _c_15_T_1 = a_15 & b_15; // @[DPIM.scala 46:22]
  wire [31:0] _c_15_T_2 = a_15 | b_15; // @[DPIM.scala 48:22]
  wire [31:0] _c_15_T_3 = a_15 ^ b_15; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_79 = io_func == 2'h2 ? _c_15_T_2 : _c_15_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_80 = io_func == 2'h1 ? _c_15_T_1 : _GEN_79; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_81 = io_func == 2'h0 ? _c_15_T : {{32'd0}, _GEN_80}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_16 = io_cme ? io_cmIn_16 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_16 = io_cme ? mem_b_16_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_16_T = a_16 * b_16; // @[DPIM.scala 44:22]
  wire [31:0] _c_16_T_1 = a_16 & b_16; // @[DPIM.scala 46:22]
  wire [31:0] _c_16_T_2 = a_16 | b_16; // @[DPIM.scala 48:22]
  wire [31:0] _c_16_T_3 = a_16 ^ b_16; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_84 = io_func == 2'h2 ? _c_16_T_2 : _c_16_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_85 = io_func == 2'h1 ? _c_16_T_1 : _GEN_84; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_86 = io_func == 2'h0 ? _c_16_T : {{32'd0}, _GEN_85}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_17 = io_cme ? io_cmIn_17 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_17 = io_cme ? mem_b_17_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_17_T = a_17 * b_17; // @[DPIM.scala 44:22]
  wire [31:0] _c_17_T_1 = a_17 & b_17; // @[DPIM.scala 46:22]
  wire [31:0] _c_17_T_2 = a_17 | b_17; // @[DPIM.scala 48:22]
  wire [31:0] _c_17_T_3 = a_17 ^ b_17; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_89 = io_func == 2'h2 ? _c_17_T_2 : _c_17_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_90 = io_func == 2'h1 ? _c_17_T_1 : _GEN_89; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_91 = io_func == 2'h0 ? _c_17_T : {{32'd0}, _GEN_90}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_18 = io_cme ? io_cmIn_18 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_18 = io_cme ? mem_b_18_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_18_T = a_18 * b_18; // @[DPIM.scala 44:22]
  wire [31:0] _c_18_T_1 = a_18 & b_18; // @[DPIM.scala 46:22]
  wire [31:0] _c_18_T_2 = a_18 | b_18; // @[DPIM.scala 48:22]
  wire [31:0] _c_18_T_3 = a_18 ^ b_18; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_94 = io_func == 2'h2 ? _c_18_T_2 : _c_18_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_95 = io_func == 2'h1 ? _c_18_T_1 : _GEN_94; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_96 = io_func == 2'h0 ? _c_18_T : {{32'd0}, _GEN_95}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_19 = io_cme ? io_cmIn_19 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_19 = io_cme ? mem_b_19_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_19_T = a_19 * b_19; // @[DPIM.scala 44:22]
  wire [31:0] _c_19_T_1 = a_19 & b_19; // @[DPIM.scala 46:22]
  wire [31:0] _c_19_T_2 = a_19 | b_19; // @[DPIM.scala 48:22]
  wire [31:0] _c_19_T_3 = a_19 ^ b_19; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_99 = io_func == 2'h2 ? _c_19_T_2 : _c_19_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_100 = io_func == 2'h1 ? _c_19_T_1 : _GEN_99; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_101 = io_func == 2'h0 ? _c_19_T : {{32'd0}, _GEN_100}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_20 = io_cme ? io_cmIn_20 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_20 = io_cme ? mem_b_20_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_20_T = a_20 * b_20; // @[DPIM.scala 44:22]
  wire [31:0] _c_20_T_1 = a_20 & b_20; // @[DPIM.scala 46:22]
  wire [31:0] _c_20_T_2 = a_20 | b_20; // @[DPIM.scala 48:22]
  wire [31:0] _c_20_T_3 = a_20 ^ b_20; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_104 = io_func == 2'h2 ? _c_20_T_2 : _c_20_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_105 = io_func == 2'h1 ? _c_20_T_1 : _GEN_104; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_106 = io_func == 2'h0 ? _c_20_T : {{32'd0}, _GEN_105}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_21 = io_cme ? io_cmIn_21 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_21 = io_cme ? mem_b_21_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_21_T = a_21 * b_21; // @[DPIM.scala 44:22]
  wire [31:0] _c_21_T_1 = a_21 & b_21; // @[DPIM.scala 46:22]
  wire [31:0] _c_21_T_2 = a_21 | b_21; // @[DPIM.scala 48:22]
  wire [31:0] _c_21_T_3 = a_21 ^ b_21; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_109 = io_func == 2'h2 ? _c_21_T_2 : _c_21_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_110 = io_func == 2'h1 ? _c_21_T_1 : _GEN_109; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_111 = io_func == 2'h0 ? _c_21_T : {{32'd0}, _GEN_110}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_22 = io_cme ? io_cmIn_22 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_22 = io_cme ? mem_b_22_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_22_T = a_22 * b_22; // @[DPIM.scala 44:22]
  wire [31:0] _c_22_T_1 = a_22 & b_22; // @[DPIM.scala 46:22]
  wire [31:0] _c_22_T_2 = a_22 | b_22; // @[DPIM.scala 48:22]
  wire [31:0] _c_22_T_3 = a_22 ^ b_22; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_114 = io_func == 2'h2 ? _c_22_T_2 : _c_22_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_115 = io_func == 2'h1 ? _c_22_T_1 : _GEN_114; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_116 = io_func == 2'h0 ? _c_22_T : {{32'd0}, _GEN_115}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_23 = io_cme ? io_cmIn_23 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_23 = io_cme ? mem_b_23_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_23_T = a_23 * b_23; // @[DPIM.scala 44:22]
  wire [31:0] _c_23_T_1 = a_23 & b_23; // @[DPIM.scala 46:22]
  wire [31:0] _c_23_T_2 = a_23 | b_23; // @[DPIM.scala 48:22]
  wire [31:0] _c_23_T_3 = a_23 ^ b_23; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_119 = io_func == 2'h2 ? _c_23_T_2 : _c_23_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_120 = io_func == 2'h1 ? _c_23_T_1 : _GEN_119; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_121 = io_func == 2'h0 ? _c_23_T : {{32'd0}, _GEN_120}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_24 = io_cme ? io_cmIn_24 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_24 = io_cme ? mem_b_24_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_24_T = a_24 * b_24; // @[DPIM.scala 44:22]
  wire [31:0] _c_24_T_1 = a_24 & b_24; // @[DPIM.scala 46:22]
  wire [31:0] _c_24_T_2 = a_24 | b_24; // @[DPIM.scala 48:22]
  wire [31:0] _c_24_T_3 = a_24 ^ b_24; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_124 = io_func == 2'h2 ? _c_24_T_2 : _c_24_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_125 = io_func == 2'h1 ? _c_24_T_1 : _GEN_124; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_126 = io_func == 2'h0 ? _c_24_T : {{32'd0}, _GEN_125}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_25 = io_cme ? io_cmIn_25 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_25 = io_cme ? mem_b_25_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_25_T = a_25 * b_25; // @[DPIM.scala 44:22]
  wire [31:0] _c_25_T_1 = a_25 & b_25; // @[DPIM.scala 46:22]
  wire [31:0] _c_25_T_2 = a_25 | b_25; // @[DPIM.scala 48:22]
  wire [31:0] _c_25_T_3 = a_25 ^ b_25; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_129 = io_func == 2'h2 ? _c_25_T_2 : _c_25_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_130 = io_func == 2'h1 ? _c_25_T_1 : _GEN_129; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_131 = io_func == 2'h0 ? _c_25_T : {{32'd0}, _GEN_130}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_26 = io_cme ? io_cmIn_26 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_26 = io_cme ? mem_b_26_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_26_T = a_26 * b_26; // @[DPIM.scala 44:22]
  wire [31:0] _c_26_T_1 = a_26 & b_26; // @[DPIM.scala 46:22]
  wire [31:0] _c_26_T_2 = a_26 | b_26; // @[DPIM.scala 48:22]
  wire [31:0] _c_26_T_3 = a_26 ^ b_26; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_134 = io_func == 2'h2 ? _c_26_T_2 : _c_26_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_135 = io_func == 2'h1 ? _c_26_T_1 : _GEN_134; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_136 = io_func == 2'h0 ? _c_26_T : {{32'd0}, _GEN_135}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_27 = io_cme ? io_cmIn_27 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_27 = io_cme ? mem_b_27_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_27_T = a_27 * b_27; // @[DPIM.scala 44:22]
  wire [31:0] _c_27_T_1 = a_27 & b_27; // @[DPIM.scala 46:22]
  wire [31:0] _c_27_T_2 = a_27 | b_27; // @[DPIM.scala 48:22]
  wire [31:0] _c_27_T_3 = a_27 ^ b_27; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_139 = io_func == 2'h2 ? _c_27_T_2 : _c_27_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_140 = io_func == 2'h1 ? _c_27_T_1 : _GEN_139; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_141 = io_func == 2'h0 ? _c_27_T : {{32'd0}, _GEN_140}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_28 = io_cme ? io_cmIn_28 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_28 = io_cme ? mem_b_28_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_28_T = a_28 * b_28; // @[DPIM.scala 44:22]
  wire [31:0] _c_28_T_1 = a_28 & b_28; // @[DPIM.scala 46:22]
  wire [31:0] _c_28_T_2 = a_28 | b_28; // @[DPIM.scala 48:22]
  wire [31:0] _c_28_T_3 = a_28 ^ b_28; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_144 = io_func == 2'h2 ? _c_28_T_2 : _c_28_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_145 = io_func == 2'h1 ? _c_28_T_1 : _GEN_144; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_146 = io_func == 2'h0 ? _c_28_T : {{32'd0}, _GEN_145}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_29 = io_cme ? io_cmIn_29 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_29 = io_cme ? mem_b_29_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_29_T = a_29 * b_29; // @[DPIM.scala 44:22]
  wire [31:0] _c_29_T_1 = a_29 & b_29; // @[DPIM.scala 46:22]
  wire [31:0] _c_29_T_2 = a_29 | b_29; // @[DPIM.scala 48:22]
  wire [31:0] _c_29_T_3 = a_29 ^ b_29; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_149 = io_func == 2'h2 ? _c_29_T_2 : _c_29_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_150 = io_func == 2'h1 ? _c_29_T_1 : _GEN_149; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_151 = io_func == 2'h0 ? _c_29_T : {{32'd0}, _GEN_150}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_30 = io_cme ? io_cmIn_30 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_30 = io_cme ? mem_b_30_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_30_T = a_30 * b_30; // @[DPIM.scala 44:22]
  wire [31:0] _c_30_T_1 = a_30 & b_30; // @[DPIM.scala 46:22]
  wire [31:0] _c_30_T_2 = a_30 | b_30; // @[DPIM.scala 48:22]
  wire [31:0] _c_30_T_3 = a_30 ^ b_30; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_154 = io_func == 2'h2 ? _c_30_T_2 : _c_30_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_155 = io_func == 2'h1 ? _c_30_T_1 : _GEN_154; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_156 = io_func == 2'h0 ? _c_30_T : {{32'd0}, _GEN_155}; // @[DPIM.scala 43:34 44:14]
  wire [31:0] a_31 = io_cme ? io_cmIn_31 : 32'h0; // @[DPIM.scala 38:16 41:12 55:12]
  wire [31:0] b_31 = io_cme ? mem_b_31_MPORT_data : 32'h0; // @[DPIM.scala 38:16 42:12 56:12]
  wire [63:0] _c_31_T = a_31 * b_31; // @[DPIM.scala 44:22]
  wire [31:0] _c_31_T_1 = a_31 & b_31; // @[DPIM.scala 46:22]
  wire [31:0] _c_31_T_2 = a_31 | b_31; // @[DPIM.scala 48:22]
  wire [31:0] _c_31_T_3 = a_31 ^ b_31; // @[DPIM.scala 50:22]
  wire [31:0] _GEN_159 = io_func == 2'h2 ? _c_31_T_2 : _c_31_T_3; // @[DPIM.scala 47:40 48:14 50:14]
  wire [31:0] _GEN_160 = io_func == 2'h1 ? _c_31_T_1 : _GEN_159; // @[DPIM.scala 45:40 46:14]
  wire [63:0] _GEN_161 = io_func == 2'h0 ? _c_31_T : {{32'd0}, _GEN_160}; // @[DPIM.scala 43:34 44:14]
  wire [63:0] _GEN_167 = io_cme ? _GEN_6 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_171 = io_cme ? _GEN_11 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_175 = io_cme ? _GEN_16 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_179 = io_cme ? _GEN_21 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_183 = io_cme ? _GEN_26 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_187 = io_cme ? _GEN_31 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_191 = io_cme ? _GEN_36 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_195 = io_cme ? _GEN_41 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_199 = io_cme ? _GEN_46 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_203 = io_cme ? _GEN_51 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_207 = io_cme ? _GEN_56 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_211 = io_cme ? _GEN_61 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_215 = io_cme ? _GEN_66 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_219 = io_cme ? _GEN_71 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_223 = io_cme ? _GEN_76 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_227 = io_cme ? _GEN_81 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_231 = io_cme ? _GEN_86 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_235 = io_cme ? _GEN_91 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_239 = io_cme ? _GEN_96 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_243 = io_cme ? _GEN_101 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_247 = io_cme ? _GEN_106 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_251 = io_cme ? _GEN_111 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_255 = io_cme ? _GEN_116 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_259 = io_cme ? _GEN_121 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_263 = io_cme ? _GEN_126 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_267 = io_cme ? _GEN_131 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_271 = io_cme ? _GEN_136 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_275 = io_cme ? _GEN_141 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_279 = io_cme ? _GEN_146 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_283 = io_cme ? _GEN_151 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_287 = io_cme ? _GEN_156 : 64'h0; // @[DPIM.scala 38:16 57:12]
  wire [63:0] _GEN_291 = io_cme ? _GEN_161 : 64'h0; // @[DPIM.scala 38:16 57:12]
  assign mem_b_0_MPORT_en = mem_b_0_MPORT_en_pipe_0;
  assign mem_b_0_MPORT_addr = mem_b_0_MPORT_addr_pipe_0;
  assign mem_b_0_MPORT_data = mem[mem_b_0_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_1_MPORT_en = mem_b_1_MPORT_en_pipe_0;
  assign mem_b_1_MPORT_addr = mem_b_1_MPORT_addr_pipe_0;
  assign mem_b_1_MPORT_data = mem[mem_b_1_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_2_MPORT_en = mem_b_2_MPORT_en_pipe_0;
  assign mem_b_2_MPORT_addr = mem_b_2_MPORT_addr_pipe_0;
  assign mem_b_2_MPORT_data = mem[mem_b_2_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_3_MPORT_en = mem_b_3_MPORT_en_pipe_0;
  assign mem_b_3_MPORT_addr = mem_b_3_MPORT_addr_pipe_0;
  assign mem_b_3_MPORT_data = mem[mem_b_3_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_4_MPORT_en = mem_b_4_MPORT_en_pipe_0;
  assign mem_b_4_MPORT_addr = mem_b_4_MPORT_addr_pipe_0;
  assign mem_b_4_MPORT_data = mem[mem_b_4_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_5_MPORT_en = mem_b_5_MPORT_en_pipe_0;
  assign mem_b_5_MPORT_addr = mem_b_5_MPORT_addr_pipe_0;
  assign mem_b_5_MPORT_data = mem[mem_b_5_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_6_MPORT_en = mem_b_6_MPORT_en_pipe_0;
  assign mem_b_6_MPORT_addr = mem_b_6_MPORT_addr_pipe_0;
  assign mem_b_6_MPORT_data = mem[mem_b_6_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_7_MPORT_en = mem_b_7_MPORT_en_pipe_0;
  assign mem_b_7_MPORT_addr = mem_b_7_MPORT_addr_pipe_0;
  assign mem_b_7_MPORT_data = mem[mem_b_7_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_8_MPORT_en = mem_b_8_MPORT_en_pipe_0;
  assign mem_b_8_MPORT_addr = mem_b_8_MPORT_addr_pipe_0;
  assign mem_b_8_MPORT_data = mem[mem_b_8_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_9_MPORT_en = mem_b_9_MPORT_en_pipe_0;
  assign mem_b_9_MPORT_addr = mem_b_9_MPORT_addr_pipe_0;
  assign mem_b_9_MPORT_data = mem[mem_b_9_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_10_MPORT_en = mem_b_10_MPORT_en_pipe_0;
  assign mem_b_10_MPORT_addr = mem_b_10_MPORT_addr_pipe_0;
  assign mem_b_10_MPORT_data = mem[mem_b_10_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_11_MPORT_en = mem_b_11_MPORT_en_pipe_0;
  assign mem_b_11_MPORT_addr = mem_b_11_MPORT_addr_pipe_0;
  assign mem_b_11_MPORT_data = mem[mem_b_11_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_12_MPORT_en = mem_b_12_MPORT_en_pipe_0;
  assign mem_b_12_MPORT_addr = mem_b_12_MPORT_addr_pipe_0;
  assign mem_b_12_MPORT_data = mem[mem_b_12_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_13_MPORT_en = mem_b_13_MPORT_en_pipe_0;
  assign mem_b_13_MPORT_addr = mem_b_13_MPORT_addr_pipe_0;
  assign mem_b_13_MPORT_data = mem[mem_b_13_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_14_MPORT_en = mem_b_14_MPORT_en_pipe_0;
  assign mem_b_14_MPORT_addr = mem_b_14_MPORT_addr_pipe_0;
  assign mem_b_14_MPORT_data = mem[mem_b_14_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_15_MPORT_en = mem_b_15_MPORT_en_pipe_0;
  assign mem_b_15_MPORT_addr = mem_b_15_MPORT_addr_pipe_0;
  assign mem_b_15_MPORT_data = mem[mem_b_15_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_16_MPORT_en = mem_b_16_MPORT_en_pipe_0;
  assign mem_b_16_MPORT_addr = mem_b_16_MPORT_addr_pipe_0;
  assign mem_b_16_MPORT_data = mem[mem_b_16_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_17_MPORT_en = mem_b_17_MPORT_en_pipe_0;
  assign mem_b_17_MPORT_addr = mem_b_17_MPORT_addr_pipe_0;
  assign mem_b_17_MPORT_data = mem[mem_b_17_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_18_MPORT_en = mem_b_18_MPORT_en_pipe_0;
  assign mem_b_18_MPORT_addr = mem_b_18_MPORT_addr_pipe_0;
  assign mem_b_18_MPORT_data = mem[mem_b_18_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_19_MPORT_en = mem_b_19_MPORT_en_pipe_0;
  assign mem_b_19_MPORT_addr = mem_b_19_MPORT_addr_pipe_0;
  assign mem_b_19_MPORT_data = mem[mem_b_19_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_20_MPORT_en = mem_b_20_MPORT_en_pipe_0;
  assign mem_b_20_MPORT_addr = mem_b_20_MPORT_addr_pipe_0;
  assign mem_b_20_MPORT_data = mem[mem_b_20_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_21_MPORT_en = mem_b_21_MPORT_en_pipe_0;
  assign mem_b_21_MPORT_addr = mem_b_21_MPORT_addr_pipe_0;
  assign mem_b_21_MPORT_data = mem[mem_b_21_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_22_MPORT_en = mem_b_22_MPORT_en_pipe_0;
  assign mem_b_22_MPORT_addr = mem_b_22_MPORT_addr_pipe_0;
  assign mem_b_22_MPORT_data = mem[mem_b_22_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_23_MPORT_en = mem_b_23_MPORT_en_pipe_0;
  assign mem_b_23_MPORT_addr = mem_b_23_MPORT_addr_pipe_0;
  assign mem_b_23_MPORT_data = mem[mem_b_23_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_24_MPORT_en = mem_b_24_MPORT_en_pipe_0;
  assign mem_b_24_MPORT_addr = mem_b_24_MPORT_addr_pipe_0;
  assign mem_b_24_MPORT_data = mem[mem_b_24_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_25_MPORT_en = mem_b_25_MPORT_en_pipe_0;
  assign mem_b_25_MPORT_addr = mem_b_25_MPORT_addr_pipe_0;
  assign mem_b_25_MPORT_data = mem[mem_b_25_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_26_MPORT_en = mem_b_26_MPORT_en_pipe_0;
  assign mem_b_26_MPORT_addr = mem_b_26_MPORT_addr_pipe_0;
  assign mem_b_26_MPORT_data = mem[mem_b_26_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_27_MPORT_en = mem_b_27_MPORT_en_pipe_0;
  assign mem_b_27_MPORT_addr = mem_b_27_MPORT_addr_pipe_0;
  assign mem_b_27_MPORT_data = mem[mem_b_27_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_28_MPORT_en = mem_b_28_MPORT_en_pipe_0;
  assign mem_b_28_MPORT_addr = mem_b_28_MPORT_addr_pipe_0;
  assign mem_b_28_MPORT_data = mem[mem_b_28_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_29_MPORT_en = mem_b_29_MPORT_en_pipe_0;
  assign mem_b_29_MPORT_addr = mem_b_29_MPORT_addr_pipe_0;
  assign mem_b_29_MPORT_data = mem[mem_b_29_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_30_MPORT_en = mem_b_30_MPORT_en_pipe_0;
  assign mem_b_30_MPORT_addr = mem_b_30_MPORT_addr_pipe_0;
  assign mem_b_30_MPORT_data = mem[mem_b_30_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_b_31_MPORT_en = mem_b_31_MPORT_en_pipe_0;
  assign mem_b_31_MPORT_addr = mem_b_31_MPORT_addr_pipe_0;
  assign mem_b_31_MPORT_data = mem[mem_b_31_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_io_q_MPORT_en = mem_io_q_MPORT_en_pipe_0;
  assign mem_io_q_MPORT_addr = mem_io_q_MPORT_addr_pipe_0;
  assign mem_io_q_MPORT_data = mem[mem_io_q_MPORT_addr]; // @[DPIM.scala 36:24]
  assign mem_MPORT_data = io_d;
  assign mem_MPORT_addr = io_addr;
  assign mem_MPORT_mask = 1'h1;
  assign mem_MPORT_en = io_we;
  assign io_q = io_we ? io_d : mem_io_q_MPORT_data; // @[DPIM.scala 67:16 69:10 71:10]
  assign io_cmOut_0 = _GEN_167[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_1 = _GEN_171[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_2 = _GEN_175[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_3 = _GEN_179[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_4 = _GEN_183[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_5 = _GEN_187[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_6 = _GEN_191[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_7 = _GEN_195[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_8 = _GEN_199[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_9 = _GEN_203[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_10 = _GEN_207[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_11 = _GEN_211[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_12 = _GEN_215[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_13 = _GEN_219[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_14 = _GEN_223[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_15 = _GEN_227[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_16 = _GEN_231[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_17 = _GEN_235[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_18 = _GEN_239[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_19 = _GEN_243[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_20 = _GEN_247[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_21 = _GEN_251[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_22 = _GEN_255[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_23 = _GEN_259[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_24 = _GEN_263[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_25 = _GEN_267[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_26 = _GEN_271[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_27 = _GEN_275[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_28 = _GEN_279[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_29 = _GEN_283[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_30 = _GEN_287[31:0]; // @[DPIM.scala 33:15]
  assign io_cmOut_31 = _GEN_291[31:0]; // @[DPIM.scala 33:15]
  always @(posedge clock) begin
    if (mem_MPORT_en & mem_MPORT_mask) begin
      mem[mem_MPORT_addr] <= mem_MPORT_data; // @[DPIM.scala 36:24]
    end
    mem_b_0_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_0_MPORT_addr_pipe_0 <= io_addr & 12'h7f;
    end
    mem_b_1_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_1_MPORT_addr_pipe_0 <= _b_0_T | 12'h80;
    end
    mem_b_2_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_2_MPORT_addr_pipe_0 <= _b_0_T | 12'h100;
    end
    mem_b_3_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_3_MPORT_addr_pipe_0 <= _b_0_T | 12'h180;
    end
    mem_b_4_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_4_MPORT_addr_pipe_0 <= _b_0_T | 12'h200;
    end
    mem_b_5_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_5_MPORT_addr_pipe_0 <= _b_0_T | 12'h280;
    end
    mem_b_6_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_6_MPORT_addr_pipe_0 <= _b_0_T | 12'h300;
    end
    mem_b_7_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_7_MPORT_addr_pipe_0 <= _b_0_T | 12'h380;
    end
    mem_b_8_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_8_MPORT_addr_pipe_0 <= _b_0_T | 12'h400;
    end
    mem_b_9_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_9_MPORT_addr_pipe_0 <= _b_0_T | 12'h480;
    end
    mem_b_10_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_10_MPORT_addr_pipe_0 <= _b_0_T | 12'h500;
    end
    mem_b_11_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_11_MPORT_addr_pipe_0 <= _b_0_T | 12'h580;
    end
    mem_b_12_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_12_MPORT_addr_pipe_0 <= _b_0_T | 12'h600;
    end
    mem_b_13_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_13_MPORT_addr_pipe_0 <= _b_0_T | 12'h680;
    end
    mem_b_14_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_14_MPORT_addr_pipe_0 <= _b_0_T | 12'h700;
    end
    mem_b_15_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_15_MPORT_addr_pipe_0 <= _b_0_T | 12'h780;
    end
    mem_b_16_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_16_MPORT_addr_pipe_0 <= _b_0_T | 12'h800;
    end
    mem_b_17_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_17_MPORT_addr_pipe_0 <= _b_0_T | 12'h880;
    end
    mem_b_18_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_18_MPORT_addr_pipe_0 <= _b_0_T | 12'h900;
    end
    mem_b_19_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_19_MPORT_addr_pipe_0 <= _b_0_T | 12'h980;
    end
    mem_b_20_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_20_MPORT_addr_pipe_0 <= _b_0_T | 12'ha00;
    end
    mem_b_21_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_21_MPORT_addr_pipe_0 <= _b_0_T | 12'ha80;
    end
    mem_b_22_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_22_MPORT_addr_pipe_0 <= _b_0_T | 12'hb00;
    end
    mem_b_23_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_23_MPORT_addr_pipe_0 <= _b_0_T | 12'hb80;
    end
    mem_b_24_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_24_MPORT_addr_pipe_0 <= _b_0_T | 12'hc00;
    end
    mem_b_25_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_25_MPORT_addr_pipe_0 <= _b_0_T | 12'hc80;
    end
    mem_b_26_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_26_MPORT_addr_pipe_0 <= _b_0_T | 12'hd00;
    end
    mem_b_27_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_27_MPORT_addr_pipe_0 <= _b_0_T | 12'hd80;
    end
    mem_b_28_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_28_MPORT_addr_pipe_0 <= _b_0_T | 12'he00;
    end
    mem_b_29_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_29_MPORT_addr_pipe_0 <= _b_0_T | 12'he80;
    end
    mem_b_30_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_30_MPORT_addr_pipe_0 <= _b_0_T | 12'hf00;
    end
    mem_b_31_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_b_31_MPORT_addr_pipe_0 <= _b_0_T | 12'hf80;
    end
    if (io_we) begin
      mem_io_q_MPORT_en_pipe_0 <= 1'h0;
    end else begin
      mem_io_q_MPORT_en_pipe_0 <= 1'h1;
    end
    if (io_we ? 1'h0 : 1'h1) begin
      mem_io_q_MPORT_addr_pipe_0 <= io_addr;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_MEM_INIT
  _RAND_0 = {1{`RANDOM}};
  for (initvar = 0; initvar < 4096; initvar = initvar+1)
    mem[initvar] = _RAND_0[31:0];
`endif // RANDOMIZE_MEM_INIT
`ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  mem_b_0_MPORT_en_pipe_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  mem_b_0_MPORT_addr_pipe_0 = _RAND_2[11:0];
  _RAND_3 = {1{`RANDOM}};
  mem_b_1_MPORT_en_pipe_0 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  mem_b_1_MPORT_addr_pipe_0 = _RAND_4[11:0];
  _RAND_5 = {1{`RANDOM}};
  mem_b_2_MPORT_en_pipe_0 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  mem_b_2_MPORT_addr_pipe_0 = _RAND_6[11:0];
  _RAND_7 = {1{`RANDOM}};
  mem_b_3_MPORT_en_pipe_0 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  mem_b_3_MPORT_addr_pipe_0 = _RAND_8[11:0];
  _RAND_9 = {1{`RANDOM}};
  mem_b_4_MPORT_en_pipe_0 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  mem_b_4_MPORT_addr_pipe_0 = _RAND_10[11:0];
  _RAND_11 = {1{`RANDOM}};
  mem_b_5_MPORT_en_pipe_0 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  mem_b_5_MPORT_addr_pipe_0 = _RAND_12[11:0];
  _RAND_13 = {1{`RANDOM}};
  mem_b_6_MPORT_en_pipe_0 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  mem_b_6_MPORT_addr_pipe_0 = _RAND_14[11:0];
  _RAND_15 = {1{`RANDOM}};
  mem_b_7_MPORT_en_pipe_0 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  mem_b_7_MPORT_addr_pipe_0 = _RAND_16[11:0];
  _RAND_17 = {1{`RANDOM}};
  mem_b_8_MPORT_en_pipe_0 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  mem_b_8_MPORT_addr_pipe_0 = _RAND_18[11:0];
  _RAND_19 = {1{`RANDOM}};
  mem_b_9_MPORT_en_pipe_0 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  mem_b_9_MPORT_addr_pipe_0 = _RAND_20[11:0];
  _RAND_21 = {1{`RANDOM}};
  mem_b_10_MPORT_en_pipe_0 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  mem_b_10_MPORT_addr_pipe_0 = _RAND_22[11:0];
  _RAND_23 = {1{`RANDOM}};
  mem_b_11_MPORT_en_pipe_0 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  mem_b_11_MPORT_addr_pipe_0 = _RAND_24[11:0];
  _RAND_25 = {1{`RANDOM}};
  mem_b_12_MPORT_en_pipe_0 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  mem_b_12_MPORT_addr_pipe_0 = _RAND_26[11:0];
  _RAND_27 = {1{`RANDOM}};
  mem_b_13_MPORT_en_pipe_0 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  mem_b_13_MPORT_addr_pipe_0 = _RAND_28[11:0];
  _RAND_29 = {1{`RANDOM}};
  mem_b_14_MPORT_en_pipe_0 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  mem_b_14_MPORT_addr_pipe_0 = _RAND_30[11:0];
  _RAND_31 = {1{`RANDOM}};
  mem_b_15_MPORT_en_pipe_0 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  mem_b_15_MPORT_addr_pipe_0 = _RAND_32[11:0];
  _RAND_33 = {1{`RANDOM}};
  mem_b_16_MPORT_en_pipe_0 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  mem_b_16_MPORT_addr_pipe_0 = _RAND_34[11:0];
  _RAND_35 = {1{`RANDOM}};
  mem_b_17_MPORT_en_pipe_0 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  mem_b_17_MPORT_addr_pipe_0 = _RAND_36[11:0];
  _RAND_37 = {1{`RANDOM}};
  mem_b_18_MPORT_en_pipe_0 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  mem_b_18_MPORT_addr_pipe_0 = _RAND_38[11:0];
  _RAND_39 = {1{`RANDOM}};
  mem_b_19_MPORT_en_pipe_0 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  mem_b_19_MPORT_addr_pipe_0 = _RAND_40[11:0];
  _RAND_41 = {1{`RANDOM}};
  mem_b_20_MPORT_en_pipe_0 = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  mem_b_20_MPORT_addr_pipe_0 = _RAND_42[11:0];
  _RAND_43 = {1{`RANDOM}};
  mem_b_21_MPORT_en_pipe_0 = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  mem_b_21_MPORT_addr_pipe_0 = _RAND_44[11:0];
  _RAND_45 = {1{`RANDOM}};
  mem_b_22_MPORT_en_pipe_0 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  mem_b_22_MPORT_addr_pipe_0 = _RAND_46[11:0];
  _RAND_47 = {1{`RANDOM}};
  mem_b_23_MPORT_en_pipe_0 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  mem_b_23_MPORT_addr_pipe_0 = _RAND_48[11:0];
  _RAND_49 = {1{`RANDOM}};
  mem_b_24_MPORT_en_pipe_0 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  mem_b_24_MPORT_addr_pipe_0 = _RAND_50[11:0];
  _RAND_51 = {1{`RANDOM}};
  mem_b_25_MPORT_en_pipe_0 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  mem_b_25_MPORT_addr_pipe_0 = _RAND_52[11:0];
  _RAND_53 = {1{`RANDOM}};
  mem_b_26_MPORT_en_pipe_0 = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  mem_b_26_MPORT_addr_pipe_0 = _RAND_54[11:0];
  _RAND_55 = {1{`RANDOM}};
  mem_b_27_MPORT_en_pipe_0 = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  mem_b_27_MPORT_addr_pipe_0 = _RAND_56[11:0];
  _RAND_57 = {1{`RANDOM}};
  mem_b_28_MPORT_en_pipe_0 = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  mem_b_28_MPORT_addr_pipe_0 = _RAND_58[11:0];
  _RAND_59 = {1{`RANDOM}};
  mem_b_29_MPORT_en_pipe_0 = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  mem_b_29_MPORT_addr_pipe_0 = _RAND_60[11:0];
  _RAND_61 = {1{`RANDOM}};
  mem_b_30_MPORT_en_pipe_0 = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  mem_b_30_MPORT_addr_pipe_0 = _RAND_62[11:0];
  _RAND_63 = {1{`RANDOM}};
  mem_b_31_MPORT_en_pipe_0 = _RAND_63[0:0];
  _RAND_64 = {1{`RANDOM}};
  mem_b_31_MPORT_addr_pipe_0 = _RAND_64[11:0];
  _RAND_65 = {1{`RANDOM}};
  mem_io_q_MPORT_en_pipe_0 = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  mem_io_q_MPORT_addr_pipe_0 = _RAND_66[11:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
