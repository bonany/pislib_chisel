/*
 * Copyright: 2022, Bonan Yan's Research Group
 * File 			    :   MemoryTester1.scala
 * Updated Time 	:   2022/12/17 00:27:47
 * Author 			  :   Bonan Yan's Group (bonanyan@pku.edu.cn)
 * Description 		:	  Test memory
 */


package pislib

import org.scalatest._
import chisel3._
import chisel3.tester._
import chisel3.experimental.BundleLiterals._

//class MemTest extends FlatSpec with ChiselScalatestTester with Matchers {
class MemTest extends FlatSpec with ChiselScalatestTester with Matchers {
  behavior of "Memory"
  it should "do something" in {
    test(new Memory(32,10)) { c =>
      c.io.d.poke("hdeadbeef".U)
      c.io.addr.poke(0.U)
      c.io.we.poke(true.B)
      c.clock.step(1)

      c.io.d.poke("habab".U)
      c.io.addr.poke(2.U)
      c.io.we.poke(true.B)
      c.clock.step(1)

      c.io.d.poke("hcdcd".U)
      c.io.addr.poke(0.U)
      c.io.we.poke(false.B)
      c.clock.step(1)
      c.io.q.expect("hdeadbeef".U(32.W))

      c.io.d.poke("h121".U)
      c.io.addr.poke(2.U)
      c.io.we.poke(false.B)
      c.clock.step(1)
      c.io.q.expect("habab".U(32.W))

      c.io.d.poke("h121".U)
      c.io.addr.poke(1.U)
      c.io.we.poke(false.B)
      c.clock.step(1)
      println(">>Last output value :" + c.io.q.peek().litValue)
      //      c.io.q.expect("habab".U(32.W))

    }
  }
}
