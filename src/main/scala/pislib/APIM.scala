/*
 * Copyright: 2022, Bonan Yan's Research Group
 * File 			    :   DPIM.scala
 * Updated Time 	:   2022/12/16 18:15:29
 * Author 			  :   Bonan Yan's Group (bonanyan@pku.edu.cn)
 * Description 		:	  This is a behavioral model for Digital PIM
 * Reference      :
 *      B. Yan et al., "A 1.041-Mb/mm2 27.38-TOPS/W Signed-INT8 Dynamic-Logic-Based ADC-less
 *      SRAM Compute-in-Memory Macro in 28nm with Reconfigurable Bitwise Operation for AI and Embedded Applications,"
 *      2022 IEEE International Solid- State Circuits Conference (ISSCC), 2022, pp. 188-190,
 *      doi: 10.1109/ISSCC42614.2022.9731545.
 */

package pislib

import chisel3._
import chisel3.util._
import chisel3.stage.ChiselStage

class APIM(DATA_WIDTH: Int = 8,
           ADDR_WIDTH: Int = 10,
           ADC_PRECISION: Int = 6, // unit: bit
           CIM_INPUT_PRECISION: Int = 4, // unit: bit
           CIM_OUTPUT_PARALLELISM: Int = 8// unit: 1 (quantity)
          ) extends Module {

  val io = IO(new Bundle {
    val addr = Input(UInt(ADDR_WIDTH.W))
    val d = Input(UInt(DATA_WIDTH.W))
    val q = Output(UInt(DATA_WIDTH.W))
    val we = Input(Bool())
    val cme = Input(Bool())
    val cmIn = Input(Vec(8, UInt(CIM_INPUT_PRECISION.W)))
    val cmOut = Output(Vec(8, UInt(ADC_PRECISION.W)))
  })

  val mem = SyncReadMem(1<<ADDR_WIDTH, UInt(DATA_WIDTH.W))

  //  define cmOut
  val cmOutTmp = Wire(Vec(8, UInt(32.W)))
  for (i <- 0 until 8) {
    when(io.cme) {
      cmOutTmp(i) :=
          io.cmIn(0) * mem.read( ((i.asUInt)<<2) | (0.U<<8) | (io.addr & "b00_111_000_11".U)) +
          io.cmIn(1) * mem.read( ((i.asUInt)<<2) | (1.U<<8) | (io.addr & "b00_111_000_11".U)) +
          io.cmIn(2) * mem.read( ((i.asUInt)<<2) | (2.U<<8) | (io.addr & "b00_111_000_11".U)) +
          io.cmIn(3) * mem.read( ((i.asUInt)<<2) | (3.U<<8) | (io.addr & "b00_111_000_11".U))
    } otherwise {
      cmOutTmp(i) := 0.U
    }
  }

  for (i <- 0 until 8) {
//    io.cmOut(i) := (cmOutTmp(i)>>8) & ("b111111".U(6.W)) // equivalent to line 55
    io.cmOut(i) := cmOutTmp(i)>>8 // ADC get MSB
  }

  //  define io.q
  when (io.we) {
    mem.write(io.addr, io.d)
    io.q := io.d //write-through
  } otherwise {
    io.q := mem.read(io.addr)
  }

} //end of module

//verilog generator
object VerilogGen_APIM extends App {
  (new ChiselStage).emitVerilog(
    new APIM(8,10,6,4,8),
    Array("--target-dir", "generated/")
  )
}



