//imported from chisel template: https://github.com/freechipsproject/chisel-template.git
ThisBuild / version          := "0.1.0"
ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / organization     := "BonanYan"

val chiselVersion = "3.5.4"

lazy val root = (project in file("."))
    .settings(
      name := "PIS_LIB",
      libraryDependencies ++= Seq(
        "edu.berkeley.cs" %% "chisel3" % chiselVersion,
        "edu.berkeley.cs" %% "chiseltest" % "0.5.4" % "test"
      ),
      scalacOptions ++= Seq(
        "-language:reflectiveCalls",
        "-deprecation",
        "-feature",
        "-Xcheckinit",
        "-P:chiselplugin:genBundleElements",
      ),
      addCompilerPlugin("edu.berkeley.cs" % "chisel3-plugin" % chiselVersion cross CrossVersion.full),
    )
